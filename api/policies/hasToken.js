
module.exports = function (req, res, next) {
  var token = req.headers['authorization'];

  if (token) {
    CipherService.verifyToken(token,function (err,data) {
      if(err) {
        res.unauthorized(null, null, 'You must provide valid token')
      }else{
        req.decoded=data;
        next();
      }
    })
  } else {
    res.unauthorized(null, null, 'You must provide application token');
  }
};
