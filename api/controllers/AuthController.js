/**
 * AuthController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

const passport = require('passport');

function _onPassportAuth(req, res, error, user, info) {
  if (error) return res.serverError(error);
  if (!user) return res.unauthorized(null, info && info.code, info && info.message);
  return res.ok({
    message: "success",
    token: CipherService.createToken(user),
    data: user
  });
}

module.exports = {

  //Signup function

  signup: function (req, res) {
    User
      .create(_.omit(req.body, 'id'))
      .then(function (user) {
        return {
          message: "success",
          data: user
        };
      })
      .then(res.created)
      .catch(function (err) {
        var code = Object.keys(err);
        if (code[1] === 'invalidAttributes') {
          var attributeData = Object.keys(err.invalidAttributes);
          var attribute = attributeData[0].charAt(0).toUpperCase() + attributeData[0].slice(1);
          return res.status(200).send({success: false, message: attribute + ' already exist!'});
        } else {
          return res.status(200).send({success: false, message: 'data error'});
        }
      });
  },

  //Login function

  signin: function (req, res) {
    passport.authenticate('local',
      _onPassportAuth.bind(this, req, res))(req, res);
  },

  //Authorization via social networks

  social: function (req, res) {
    var type = req.param('type') ? req.param('type').toLowerCase() : '-';
    var strategyName = [type, 'token'].join('-');

    if (Object.keys(passport._strategies).indexOf(strategyName) === -1) {
      return res.badRequest(null, null, [type.slice(0, 1).toUpperCase(), type.slice(1), ' is not supported'].join(''));
    }

    passport.authenticate('jwt', function (error, user) {
      req.user = user;
      passport.authenticate(strategyName, _onPassportAuth.bind(this, req, res))(req, res);
    })(req, res);
  },

// Accept JSON Web Token and updates with new one

  refresh_token: function (req, res) {
    res.badRequest(null, null, 'Not implemented yet');
  }

};
