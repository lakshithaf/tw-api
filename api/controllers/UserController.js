/**
 * UserController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

module.exports = {
  getUser: function (req, res) {
    res.send(JSON.stringify(req.decoded))
  },
}
